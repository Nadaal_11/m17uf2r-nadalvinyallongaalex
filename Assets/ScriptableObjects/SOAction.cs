using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SOAction : ScriptableObject
{
    public int value;
    public abstract void Use();
}
