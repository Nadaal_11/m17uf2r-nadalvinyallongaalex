using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Action", menuName = "My ScriptableObjects/Create Action/Speed")]
public class Speed : SOAction
{
    public static event Action<int> SpeedUp = delegate { };
    public override void Use()
    {
        SpeedUp.Invoke(value);
    }
}
