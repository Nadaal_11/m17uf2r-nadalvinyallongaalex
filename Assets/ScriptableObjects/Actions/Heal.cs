using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Action", menuName = "My ScriptableObjects/Create Action/Heal")]
public class Heal : SOAction
{
    public override void Use()
    {
        PlayerStats.playerStats.HealCharacter(value);
    }
}
