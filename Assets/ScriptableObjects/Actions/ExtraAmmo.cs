using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Action", menuName = "My ScriptableObjects/Create Action/Ammo")]

public class ExtraAmmo : SOAction
{
    public static event Action<int> AmmoUp = delegate { };
    public override void Use()
    {
        AmmoUp.Invoke(value);
    }
}
