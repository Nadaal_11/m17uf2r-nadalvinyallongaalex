using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatMakeDamage : MonoBehaviour
{
    public float damage;
    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PlayerStats.playerStats.DealDamage(damage);
            StartCoroutine(WaitSeconds());
        }

    }
    IEnumerator WaitSeconds()
    {
        _animator.SetTrigger("HitPlayer");
        Destroy(GetComponent<BoxCollider2D>());
        yield return new WaitForSeconds(0.5f);
        RoomController._instance.AddEnemieKilled();
        Destroy(this.gameObject);
    }
}
