using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public float damage;
    public AudioClip sound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Enemy")
        {
            if (collision.tag == "Hitbox")
            {
                PlayerStats.playerStats.DealDamage(damage);
                StartCoroutine(WaitForSound());
            }
           
        }
    }
    private IEnumerator WaitForSound()
    {
        GetComponent<AudioSource>().PlayOneShot(sound);
        GetComponent<Animator>().SetTrigger("Hit");
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
