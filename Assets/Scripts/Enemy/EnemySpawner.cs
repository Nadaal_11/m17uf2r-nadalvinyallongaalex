using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System;
public class EnemySpawner : MonoBehaviour
{
    public static event Action<RoomController> changeRoomController = delegate { };
    public List<GameObject> Enemies = new List<GameObject>();
    public int maxEnemies;
    public float spawnInterval;

    public RoomController roomController;
    private BoxCollider2D _boxCollider;
    private float timer = 0;
    private int currentEnemies;
    private float[] _positionXForRange;
    private float[] _positionYForRange;

    public void Start()
    {
        maxEnemies = RoomController._instance.enemiesSpawn;
        _boxCollider = GetComponent<BoxCollider2D>();
        Vector2 size = _boxCollider.size;
        Vector3 worldPos = transform.TransformPoint(_boxCollider.offset);
        _positionXForRange = new float[] { worldPos.x - (size.x / 2f), worldPos.x +  (size.x / 2f)};
        _positionYForRange = new float[] { worldPos.y - (size.y / 2f), worldPos.y + (size.y / 2f) };
       
    }
    private void Update()
    {
        timer += Time.deltaTime;
        //SpawnEnemy();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player" /*&& !RoomController._instance.checkDoor*/)
        {
            changeRoomController.Invoke(roomController);
            SpawnEnemy();

        }
    }
    /*private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Player")
        {
            currentEnemies = 0;
        }
    }*/
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.name == "Player"/* && !RoomController._instance.checkDoor*/)
        {
            SpawnEnemy();
        }
    }
    private void SpawnEnemy()
    {

        if (timer >= spawnInterval && currentEnemies < maxEnemies)
        {
            int rnd = UnityEngine.Random.Range(0, Enemies.Count);
            Instantiate(Enemies[rnd], new Vector3(UnityEngine.Random.Range(_positionXForRange[0], _positionXForRange[1]), UnityEngine.Random.Range(_positionYForRange[0], _positionYForRange[1]), 0), Quaternion.identity);
            timer = 0f;
            currentEnemies++;
        }
       }
}
