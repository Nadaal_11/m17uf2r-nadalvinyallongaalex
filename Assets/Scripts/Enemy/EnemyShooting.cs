using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public GameObject player;
    public GameObject projectile;
    public float minDamage;
    public float maxDamage;
    public float projectileForce;
    public float cooldown;
    public float range;

    public void Start()
    {
        player = FindObjectOfType<PlayerMovement>().gameObject;
        StartCoroutine(ShootPlayer());
    }
    IEnumerator ShootPlayer()
    {
        yield return new WaitForSeconds(cooldown);
        if (player != null)
        {
            float distanceFromPlayer = Vector2.Distance(player.transform.position, transform.position);
            if (distanceFromPlayer < range)
            {
                GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity);
                Vector2 myPos = transform.position;
                Vector2 playerPos = player.transform.position;
                Vector2 direction = (playerPos - myPos).normalized;
                bullet.GetComponent<Rigidbody2D>().velocity = direction * projectileForce;
                bullet.GetComponent<EnemyProjectile>().damage = Random.Range(minDamage, maxDamage);

            }       
            StartCoroutine(ShootPlayer());
        }    
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
