using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyGetDamage : MonoBehaviour
{
    public float health;
    public float maxHealth;
    public GameObject healthBar;
    public Slider healthBarSlider;

    public GameObject lootDrop;

    void Start()
    {
        health = maxHealth;
    }  

    public void DealDamage(float damage)
    {
        healthBar.SetActive(true);
        health -= damage;
        CheckDeath();
        healthBarSlider.value = CalculateHealthPercentage();
    }
    public void HealCharacter(float heal)
    {
        health += heal;
        CheckOverheal();
        healthBarSlider.value = CalculateHealthPercentage();
    }
    private void CheckOverheal()
    {
        if (health>maxHealth)
        {
            health = maxHealth;
        }
    }
    private void CheckDeath()
    {
        if (health<=0)
        {
            RoomController._instance.AddEnemieKilled();
            RoomController._instance.AddScore(2);
            Destroy(gameObject);
            int rnd = Random.Range(0, 2);
            if (rnd == 1)
            {
                Instantiate(lootDrop, transform.position, Quaternion.identity);
            }
            
        }
    }
    private float CalculateHealthPercentage()
    {
        return (health / maxHealth);
    }
}
