using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDropper : MonoBehaviour
{
    public WeaponDataSO[] armas;
    private void OnDestroy()
    {
        int random = Random.Range(0, armas.Length);
        gameObject.GetComponent<SpriteRenderer>().sprite = armas[random].DefaultItemSprite;
    }
}
