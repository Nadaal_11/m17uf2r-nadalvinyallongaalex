using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjects : MonoBehaviour
{
    public float health;
    public float maxHealth;
    public WeaponDataSO[] armas;
    public GameObject weaponDisplayer;
    void Start()
    {
        health = maxHealth;
    }

    public void DealDamage(float damage)
    {
        health -= damage;
        CheckDeath();
    }
    private void CheckDeath()
    {
        if (health < 0)
        {
            int random = Random.Range(0, armas.Length);
            var weapon = Instantiate(weaponDisplayer, this.GetComponent<Transform>().position, Quaternion.identity);
            weapon.GetComponent<WeaponLootController>().LoadWeapon(armas[random]);
            Destroy(gameObject);
        }
    }
}
