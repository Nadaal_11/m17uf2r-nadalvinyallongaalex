using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopTemplate : MonoBehaviour
{
    public Text ItemName;
    public Text ItemCost;
    public Image ItemImage; 
}
