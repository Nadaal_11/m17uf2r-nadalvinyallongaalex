using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class ItemData : ScriptableObject
{
    [SerializeField]
    private string _name;
    public string Name { get { return _name; } }

    public AnimatorController animatorController;

    [SerializeField]
    private string _description;
    public string Description { get { return _description; } }

    public Sprite ItemIcon;
    public Sprite DefaultItemSprite;
   
}   