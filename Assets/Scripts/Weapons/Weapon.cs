using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ScriptableObjects;

public class Weapon : SlotController
{
    public WeaponDataSO weaponData;
    protected float time;
    Vector3 lookPos;
    public Text textBullets;
    public Text textRecharge;
    public int inventoryIndex = 0;
    //private Animator _animator;

    public void Start()
    {
        //_animator = this.gameObject.GetComponent<Animator>();
    }
    public override void OnChange()
    {
        ExtraAmmo.AmmoUp -= AddAmmo;
    }
    public override void OnAwake()
    {
        ExtraAmmo.AmmoUp += AddAmmo;
        weaponData = (WeaponDataSO)currentItem;
        weaponData.isIniatilize();
    }

    public override void OnUpdate()
    {
        //SetAnimator();
        weaponData = (WeaponDataSO)currentItem;
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        lookPos = Camera.main.ScreenToWorldPoint(mousePos) - transform.position;
        if (Input.GetMouseButtonDown(0) && weaponData._currentAmmo != 0)
        {
            Shoot();
            weaponData._currentAmmo--;
        }
    }

    public override void OnLateUpdate()
    {
        if (weaponData._currentAmmo == 0 && weaponData._maxAmmo != 0 || Input.GetKey(KeyCode.R))
        {
            time += Time.deltaTime;
            textRecharge.text = "Recharging...";
            if (weaponData.timeToRecharge < time)
            {
                weaponData._currentAmmo = weaponData._maxAmmoLoadder;
                weaponData._maxAmmo -= weaponData._maxAmmoLoadder;
                time = 0;
                textRecharge.text = " ";
            }
        }
        textBullets.text = weaponData._currentAmmo.ToString() + " | " + weaponData._maxAmmo.ToString();
    }
    public void AddAmmo(int ammo)
    {
        weaponData._maxAmmo += ammo;
    }
    private void Shoot()
    {
        //_animator.SetTrigger("Shot");
        GameObject tempBullet = Instantiate(weaponData.bulletPF, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation) as GameObject;
        Rigidbody2D tempRigidBodyBullet = tempBullet.GetComponent<Rigidbody2D>();
        tempRigidBodyBullet.velocity = lookPos.normalized * weaponData.bulletspeed;
        // tempRigidBodyBullet.AddForce(lookPos.normalized * weaponData.BulletSpead);
    }
   /* public void SetAnimator()
    {
        _animator.runtimeAnimatorController = weaponData.animatorController;
    }*/
    
   /* void Initialize()
    {
        for (int i = 0; i < inventory.slots.Length; i++)
        {
            if (inventory.slots[i] != null)
            {
                inventory.slots[i].ClearBullets();
                inventory.slots[i].isIniatilize();
            }
        }
    }*/
}
