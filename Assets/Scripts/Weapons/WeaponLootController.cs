using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScriptableObjects;

public class WeaponLootController : MonoBehaviour
{
    public Inventory inventory;
    public WeaponDataSO arma;

    public void LoadWeapon(WeaponDataSO arma)
    {
        this.arma = arma;
        gameObject.GetComponent<SpriteRenderer>().sprite = arma.DefaultItemSprite;
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Debug.Log(arma.name + " arma que recullo");
            bool trobat = false;
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                try
                {
                    if (inventory.slots[i] == arma)
                    {
                        Debug.Log("ja tinc l'arma");
                        var tal = (WeaponDataSO)inventory.slots[i];
                        tal.isIniatilize();
                        trobat = true;
                        break;
                    }
                }
                catch { }
            }
            if(!trobat)
            {
                inventory.AddItem(arma);
                arma.isIniatilize();
            }
            Destroy(gameObject);

        }
    }
}
