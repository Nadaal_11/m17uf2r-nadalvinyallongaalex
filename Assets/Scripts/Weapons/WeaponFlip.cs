using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFlip : MonoBehaviour
{
    private GameObject player;
    private void Start()
    {
        player = GameObject.Find("Player");
    }
    void Update()
    {
        var mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        var playerScreenPoint = Camera.main.WorldToScreenPoint(player.transform.position);
        if (mouse.x < playerScreenPoint.x)
        {
            gameObject.GetComponent<SpriteRenderer>().flipY = true;
        }
        else if (mouse.x > playerScreenPoint.x)
        {
            gameObject.GetComponent<SpriteRenderer>().flipY = false;
        }
    }
}
