using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{
    public static RoomController _instance;
    public int enemiesKilled;
    public int score =0;
    public int enemiesSpawn;
    public GameObject portaAdalt;
    public GameObject portaAbaix;
    public bool checkDoor = false;

    public static RoomController Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Room Controller is NULL!");
            }
            return _instance;
        }
        
    }
    public void ChangeInstance(RoomController roomController)
    {
        _instance = roomController;
    }
    private void OnEnable()
    {
        EnemySpawner.changeRoomController += ChangeInstance;
    }
    private void OnDisable()
    {
        EnemySpawner.changeRoomController -= ChangeInstance;
    }
    private void Awake()
    {
        _instance = this;
    }
    public void Update()
    {
        CheckIfEnemiesAreDead();
    }

    public void AddEnemieKilled()
    {
        enemiesKilled += 1;
        Debug.Log(enemiesKilled + " ENEMIES KILLED");
    }
    public void AddScore(int i)
    {
        score += i;
    }
    public void CheckIfEnemiesAreDead()
    {
        if (enemiesKilled == enemiesSpawn && checkDoor == false)
        {
            checkDoor = true;
            int num = Random.Range(0, 2);
            Debug.Log(num + "NUUUM");
            if (num == 0)
            { 
                portaAdalt.SetActive(false);
                Debug.Log("Porta oberta");
            }
            if (num == 1)
            {
                portaAbaix.SetActive(false);
                Debug.Log("Porta oberta");
            }
        }
    }
}
