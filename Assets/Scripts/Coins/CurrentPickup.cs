using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


public class CurrentPickup : MonoBehaviour
{
    public int pickUpQuantity = 0;
    public AudioClip sound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player")
        {
            GameManager.Instance.coins += pickUpQuantity;
            GameManager.Instance.AddCoins();
            StartCoroutine(WaitForSound());
        }
    }
    private IEnumerator WaitForSound()
    {
        GetComponent<AudioSource>().PlayOneShot(sound);
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
