using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Results 
{
    public string Id;
    public int Score;
    public DateTime CurrentDateTime;

    public Results(int score)
    {
        CurrentDateTime = DateTime.Now;
        Id = GenerateId();
        Score = score;

    }
    private string GenerateId()
    {
        return $"{Score}{CurrentDateTime.Day}{CurrentDateTime.Month}{CurrentDateTime.Year}";
    }
    public override string ToString()
    {
        return $"{Id}, {Score}, {CurrentDateTime}";
    }
  
}
