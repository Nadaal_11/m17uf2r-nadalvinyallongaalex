using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenJson : MonoBehaviour
{
    private List<Results> results;
    public Text scoreText;

    private void Start()
    {
        string json = System.IO.File.ReadAllText("Assets/JSONs/SavedTestData.json");
        results = JsonConvert.DeserializeObject<List<Results>>(json);

        Results highResult = results[0];
        foreach (Results r in results)
        {
            if (r.Score > highResult.Score)
            {
                highResult = r;
            }
        }
        scoreText.text = "HIGH SCORE: "+highResult.Score.ToString();
    }
}
