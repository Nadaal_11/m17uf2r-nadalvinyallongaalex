using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveToJson : MonoBehaviour
{
   public void SaveIntoJson(int score)
    {
        string jsonFile = "Assets/JSONs/SavedTestData.json";
        List<Results> results = new List<Results>();
        var currentSavedData = JsonConvert.DeserializeObject<List<Results>>(File.ReadAllText(jsonFile));
        if (currentSavedData != null) results = currentSavedData;
        results.Add(new Results(score));

        using (StreamWriter sw = new StreamWriter(jsonFile))
        {
            sw.WriteLine("[");
            for (int i = 0; i < results.Count; i++)
            {
                sw.WriteLine(JsonConvert.SerializeObject(results[i]));
                if (i != results.Count - 1) sw.WriteLine(",");
            }
            sw.WriteLine("]");
        }
    }
}
