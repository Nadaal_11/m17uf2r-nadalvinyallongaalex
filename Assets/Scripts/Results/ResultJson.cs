using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultJson : MonoBehaviour
{
    private List<Results> results;
    public Text scoreText;

    private void Start()
    {
        string json = System.IO.File.ReadAllText("Assets/JSONs/SavedTestData.json");
        results = JsonConvert.DeserializeObject<List<Results>>(json);

        int last = results.Count - 1;
        scoreText.text = "Score: " + results[last].Score.ToString();
    }
}