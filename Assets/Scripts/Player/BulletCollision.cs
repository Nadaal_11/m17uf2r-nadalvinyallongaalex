using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour
{
    public float damage;
    public AudioClip sound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if (collision.GetComponent<EnemyGetDamage>() != null)
            {
                collision.GetComponent<EnemyGetDamage>().DealDamage(damage);
            }
            StartCoroutine(WaitForSound());
        }
        if (collision.tag == "wall")
        {
            Destroy(gameObject);
        }
        if (collision.tag == "Object")
        {
            if (collision.GetComponent<DestroyObjects>() != null)
            {
                collision.GetComponent<DestroyObjects>().DealDamage(damage);
            }
            StartCoroutine(WaitForSound());
        }
    }
    private IEnumerator WaitForSound()
    {
        GetComponent<AudioSource>().PlayOneShot(sound);
        Destroy(GetComponent<CircleCollider2D>());
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }
}
