using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public void OnEnable()
    {
        Speed.SpeedUp += IncreaseSpeed;
    }
    public void OnDisable()
    {
        Speed.SpeedUp -= IncreaseSpeed;
    }
    public int speed;
    public float dashRange;
    private Vector2 _direction;
    private Rigidbody2D _rb;
    private Animator _animator;
    private bool space;
    private bool dashing = false;
    public Slider dashSlider;
    private bool _hitted;

    public void IncreaseSpeed(int value)
    {
        speed += value;
    }
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void Update()
    {
        TakeInput();
        if (space)
        {
            if (!dashing)
            {
                StartCoroutine(Dash());
            }
        }
        else
        {
            Move();
        }
        SetAnimator();
    }
    private void Move()
    {
        _rb.velocity = new Vector2(_direction.x * speed, _direction.y * speed);
    }
    private IEnumerator Dash()
    { 
        _rb.velocity = _direction *dashRange;
        dashSlider.value = 0.1f;
        dashing = true;
        yield return new WaitForSeconds(2f);
        dashSlider.value = 1f;
        dashing = false;     
    }
    private void TakeInput()
    {
        space = Input.GetKey(KeyCode.Space);
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        _direction = new Vector2(moveX, moveY).normalized;
        
    }
    private void SetAnimator()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition -= transform.position;
        mousePosition =  mousePosition.normalized;
        if (!_hitted)
        {
            if (_rb.velocity != Vector2.zero)
            {
                _animator.Play("PlayerWalk");
            }
            else _animator.Play("IdleTree");
        }
        
        _animator.SetFloat("DirX", mousePosition.x);
        _animator.SetFloat("DirY", mousePosition.y);
    }

    public void Hitted()
    {
        _animator.Play("HitPlayer");
        _hitted = true;

    }

    public void EndHitted()
    {
        _hitted = false;

    }
}
