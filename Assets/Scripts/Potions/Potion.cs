using ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potion : SlotController
{
    //private Animator _animator;
    private PotionSO potionSO;
    public Inventory inventory;
    public override void OnAwake()
    {
        //_animator = this.gameObject.GetComponent<Animator>();
    }

    public override void OnChange()
    {
     
    }

    public override void OnLateUpdate()
    {
     
    }

    public override void OnUpdate()
    {
        //SetAnimator();
        potionSO = (PotionSO)currentItem;
        if (Input.GetMouseButtonDown(0))
        {
            potionSO.Use();
            inventory.DeleteItem(potionSO);
        }
    }
    /*public void SetAnimator() {
        _animator.runtimeAnimatorController = potionSO.animatorController;
    }*/
}
