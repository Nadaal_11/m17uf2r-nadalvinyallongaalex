using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Potion", menuName = "My ScriptableObjects/Create Potion", order = 1)]
public class PotionSO : ItemData
{
    public int cost;
    public SOAction SOAction;
    
    public void Use()
    {
        SOAction.Use();
    }
   
}
