using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public int coins;
    public int kills = 0;
    public int score = 0;
    public Text coinsText;
    public Text killsText;
    public Text scoreText;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this;
        AddCoins();
    }
    public void Update()
    {
        if (killsText != null)
        {
            AddKills();
        }
        if (scoreText != null)
        {
            AddScore();
        }
        
    }
    public void AddCoins() //canviar per checkpoints
    {
        coinsText.text = coins.ToString();
        ShopManager.Instance.CheckPurchaseable();
    }
    public void AddKills()
    {
        kills = RoomController._instance.enemiesKilled;
        killsText.text = kills.ToString();
    }
    public void AddScore()
    {
        score = RoomController._instance.score;
        scoreText.text = score.ToString();

    }
    public void SaveResultsJson() 
    { 
        SaveToJson saveResults = new SaveToJson();
        saveResults.SaveIntoJson(score);
    }

}
