using ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotManager : MonoBehaviour
{
    public SlotController currentSlotController;
    public ItemData currentItem;
    public Inventory inventory;
    public int inventoryIndex = 0;

    private void Awake()
    {
        //inventory.InventoryStart();
        currentItem = inventory.slots[inventoryIndex];
        gameObject.GetComponent<SpriteRenderer>().sprite = currentItem.DefaultItemSprite;
        CheckSlotController();
        currentSlotController.OnAwake();

    }
    public void CheckSlotController()
    {
        if (currentItem.GetType() == typeof(WeaponDataSO))
        {
            currentSlotController = GetComponent<Weapon>();
        }
        else
        {
            currentSlotController = GetComponent<Potion>();
        }
        currentSlotController.currentItem = currentItem;
    }
    private void Update()
    {
        ChangeWeapon();
        currentSlotController.OnUpdate();
    }
    private void LateUpdate()
    {
        currentSlotController.OnLateUpdate();
    }
    private void ChangeWeapon()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            inventoryIndex++;
            CheckIndex();
            Debug.Log(inventoryIndex);
            currentItem = inventory.slots[inventoryIndex];
            CheckSlotController();
            gameObject.GetComponent<SpriteRenderer>().sprite = currentItem.DefaultItemSprite;
        }
    }
    private void CheckIndex()
    {
        if (inventoryIndex > 4)
        {
            inventoryIndex = 0;
        }
        else if (inventory.slots[inventoryIndex] == null)
        {
            if (inventoryIndex + 1 > 4)
            {
                inventoryIndex = 0;
            }
            else if (inventory.slots[inventoryIndex+1] != null)
            {
                inventoryIndex += 1;
            }
            else
            {
                inventoryIndex = 0;
            }
        }
    }
}
