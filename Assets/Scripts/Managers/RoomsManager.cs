using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomsManager : MonoBehaviour
{
    public static RoomsManager _instance;
    public static RoomsManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Room Manager is NULL!");
            }
            return _instance;
        }
    }
}
