using ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    //public int coins;
    public Text coinsText;
    public PotionSO[] shopItems;
    public ShopTemplate[] shopPanels;
    public Button[] myPurchaseBtns;
    public Inventory inventory;

    private static ShopManager _instance;
    public static ShopManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Shop Manager is NULL!");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this;
    }
    void Start()
    { 
        LoadPanels();
    }
    private void Update()
    {
        coinsText.text = GameManager.Instance.coins.ToString();
    }
    public void PurchaseItem(int btnNum)
    {
        if (GameManager.Instance.coins >= shopItems[btnNum].cost)
        {
            GameManager.Instance.coins = GameManager.Instance.coins - shopItems[btnNum].cost;
            coinsText.text = GameManager.Instance.coins.ToString();
            CheckPurchaseable();
            inventory.AddItem(shopItems[btnNum]);

        }
    }
    public void LoadPanels()
    {
        for (int i = 0; i < shopItems.Length; i++)
        {
            shopPanels[i].ItemName.text = shopItems[i].Name;
            shopPanels[i].ItemCost.text = shopItems[i].cost.ToString();
            shopPanels[i].ItemImage.sprite = shopItems[i].DefaultItemSprite;
        }
    }
    public void CheckPurchaseable()
    {
        Debug.Log(GameManager.Instance.coins + "!");
        for (int i = 0; i < shopPanels.Length; i++)
        {

            if (GameManager.Instance.coins >= shopItems[i].cost)
            {
                myPurchaseBtns[i].interactable = true;
            }
            else myPurchaseBtns[i].interactable = false;
        }
    }

    
}
