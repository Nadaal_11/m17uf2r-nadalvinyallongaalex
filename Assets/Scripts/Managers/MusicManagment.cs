using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MusicManagment : MonoBehaviour
{
    public Slider MusicVolume;
    public Slider SFXVolume;
    public AudioMixer musicMixer;
    public AudioMixer SFXMixer;

    void Update()
    {
        if (MusicVolume != null && musicMixer != null)
        {
            MusicVolume.onValueChanged.AddListener(ChangeMusic);
        }
        if (SFXMixer !=null && SFXVolume != null)
        {
            SFXVolume.onValueChanged.AddListener(ChangeSFX);
        }
        
    }

    public void ChangeMusic(float v)
    {
        musicMixer.SetFloat("ControlMusic", v);
    }
    public void ChangeSFX(float v)
    {
        SFXMixer.SetFloat("ControlSFX", v);
    }
}
