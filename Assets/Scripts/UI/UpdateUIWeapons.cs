using ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateUIWeapons : MonoBehaviour
{
    public Inventory inventory;
    public Image[] images;
    public Sprite defaultSprite;
    // Start is called before the first frame update
    void Update()
    {
        UpdateWeaponsUI();
    }

    // Update is called once per frame
    public void UpdateWeaponsUI()
    {
        for (int i = 0; i < inventory.slots.Length; i++)
        {
            if (inventory.slots[i] != null)
            {
                images[i].sprite = inventory.slots[i].DefaultItemSprite;
            }
            else
            {
                images[i].sprite = defaultSprite;
            }
        }
    }
}
